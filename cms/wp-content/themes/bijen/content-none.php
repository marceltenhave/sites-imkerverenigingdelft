<?php
/**
 * The template for displaying a "No posts found" message.
 */
?>
<div class="post-notfound">
	<h1 class="title">
	  <?php _e('Niets gevonden','advertica-lite'); ?>
	</h1>
</div>
<div class="page-content">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
		<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'advertica-lite' ), admin_url( 'post-new.php' ) ); ?></p>
	<?php elseif ( is_search() ) : ?>
		<p><?php _e( 'Sorry, maar we hebben niets kunnen vinden met uw zoekterm. Probeer a.u.b. met andere sleutelwoorden.', 'advertica-lite' ); ?></p>
		<br><?php get_search_form(); ?>
	<?php else : ?>
		<p><?php _e( 'Het lijkt er op dat we niet kunnen vinden waar u naar zoekt. Wellicht dat zoeken kan helpen.', 'advertica-lite' ); ?></p>
		<br><?php get_search_form(); ?>
	<?php endif; ?>
</div><!-- page-content -->