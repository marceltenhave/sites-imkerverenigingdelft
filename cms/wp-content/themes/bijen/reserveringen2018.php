<?php 
/**
* Template Name: reserveringen2018
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages and that other
* 'pages' on your WordPress site will use a different template.
*
*/
get_header(); ?>

<div class="main-wrapper-item"> 

		<div class="bread-title-holder">
			<div class="bread-title-bg-image full-bg-breadimage-fixed"></div>
			<div class="container">
				<div class="row-fluid">
					<div class="container_inner clearfix">
						<h1 class="title"><?php the_title(); ?></h1>

					</div>
				</div>
			</div>
		</div>

	<div class="page-content default-pagetemp">
		<div class="container post-wrap">
			<div class="row-fluid">
				<div id="content" class="span8">
					<div class="post clearfix" id="post-<?php the_ID(); ?>">
						<div class="skepost">


<?php require_once('Connections/lokaal_ddd.php'); ?>
<?php
if (!function_exists("GetSQLiValueString")) {
function GetSQLiValueString($theConnection, $theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysqli_real_escape_string") ? mysqli_real_escape_string($theConnection, $theValue) : mysqli_escape_string($theConnection, $theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}
?>

<h2>#1: Honingslinger met motor</h2>


<?php
//**************************
// ProfRecWiz - MySqli
// http://www.dwzone-it.com
// Version: 1.0.15
//**************************
mysqli_select_db($lokaal_ddd, $database_lokaal_ddd);
$mysqli_Recordset1_query = "SELECT bijen_wpdevart_reservations.calendar_id, bijen_wpdevart_reservations.check_in, bijen_wpdevart_reservations.check_out, bijen_wpdevart_reservations.form FROM bijen_wpdevart_reservations WHERE (((bijen_wpdevart_reservations.calendar_id)=1) AND ((bijen_wpdevart_reservations.check_in)>" .GetSQLiValueString($lokaal_ddd, "2018-01-01", "text") .")) ORDER BY bijen_wpdevart_reservations.check_in Desc";
$Recordset1 = mysqli_query($lokaal_ddd, $mysqli_Recordset1_query) or die(mysqli_error($lokaal_ddd));
$row_Recordset1 = mysqli_fetch_assoc($Recordset1);
$totalRows_Recordset1 = mysqli_num_rows($Recordset1);
//**************************
// ProfRecWiz - MySqli
//**************************
?>

<table width="810" border="1">    
  <tr>      
      <th width="55" height="30" scope="col"><strong>#</strong></th>
      <th width="136" height="30" scope="col"><strong>Begindatum</strong></th>
      <th width="134" height="30" scope="col"><strong>Einddatum</strong></th>
      <th width="238" height="30" scope="col"><strong>Naam</strong></th>
      <th width="213" height="30" scope="col"><strong>Telefoon</strong></th>
  </tr>
  <?php
//*****************************
// ProfRecWiz
// Repeat Region
// Before Selection Instance
//*****************************
$dwz_loop = 0;
$dwzLoopName = "Repeat1";
$dwzMaxRec = "-1";
do{
$dwz_loop += 1;
//End Repeat region
?>  
<?php
$json = dwzGetRecValue('Recordset1','form');
$obj = json_decode($json);
$checkin = dwzGetRecValue('Recordset1','check_in');
$checkout = dwzGetRecValue('Recordset1','check_out');
?>
<tr>
      <th width="55" height="30" scope="col"><?php echo dwzGetRecValue('Recordset1','calendar_id'); ?></th>
      <th width="136" height="30" scope="col"><?php echo date('d-m-Y', strtotime($checkin)); ?></th>
      <th width="134" height="30" scope="col"><?php echo date('d-m-Y', strtotime($checkout)); ?></th>
      <th width="238" height="30" scope="col">
	  <?php echo $obj->{'wpdevart_form_field1'}; ?>
	  <?php echo $obj->{'wpdevart_form_field2'}; ?></th>
      <th width="213" height="30" scope="col"><?php echo $obj->{'wpdevart_form_field4'}; ?></th>
    </tr>
    <?php
//*****************************
// ProfRecWiz
// Repeat Region
// After Selection Instance
//*****************************
$dwzLoopName = "Repeat1";
if($dwzMaxRec != "" && $dwzMaxRec != "-1"){
	if(intval($dwz_loop) >= intval($dwzMaxRec)){
		break;
	}
}
}while($row_Recordset1 = mysqli_fetch_assoc($Recordset1));
//End Repeat region
?>
  <tr>
    
  </tr>
</table>
<p>&nbsp;</p>

<?php
//**************************
// ProfRecWiz - MySqli
//**************************
mysqli_free_result($Recordset1);
//**************************
// ProfRecWiz - MySqli
//**************************
?>
<?php 
//**************************
// ProfRecWiz
//**************************
function dwzGetRecValue($rec, $field){
	if(isset($GLOBALS['row_'.$rec])){
		return $GLOBALS['row_'.$rec][$field];
	}else{
		return $rec .'.' .$field;
	}
}
//**************************
// ProfRecWiz
//**************************

?>

<h2>#2: Kleine honingslinger</h2>

<?php
//**************************
// ProfRecWiz - MySqli
// http://www.dwzone-it.com
// Version: 1.0.15
//**************************
mysqli_select_db($lokaal_ddd, $database_lokaal_ddd);
$mysqli_Recordset2_query = "SELECT bijen_wpdevart_reservations.calendar_id, bijen_wpdevart_reservations.check_in, bijen_wpdevart_reservations.check_out, bijen_wpdevart_reservations.form FROM bijen_wpdevart_reservations WHERE (((bijen_wpdevart_reservations.calendar_id)=2) AND ((bijen_wpdevart_reservations.check_in)>" .GetSQLiValueString($lokaal_ddd, "2018-01-01", "text") .")) ORDER BY bijen_wpdevart_reservations.check_in Desc";
$Recordset2 = mysqli_query($lokaal_ddd, $mysqli_Recordset2_query) or die(mysqli_error($lokaal_ddd));
$row_Recordset2 = mysqli_fetch_assoc($Recordset2);
$totalRows_Recordset2 = mysqli_num_rows($Recordset2);
//**************************
// ProfRecWiz - MySqli
//**************************
?>

<table width="810" border="1">
  <tr>      
      <th width="55" height="30" scope="col"><strong>#</strong></th>
      <th width="138" height="30" scope="col"><strong>Begindatum</strong></th>
      <th width="132" height="30" scope="col"><strong>Einddatum</strong></th>
      <th width="238" height="30" scope="col"><strong>Naam</strong></th>
      <th width="213" height="30" scope="col"><strong>Telefoon</strong></th>
  </tr>
  <?php
//*****************************
// ProfRecWiz
// Repeat Region
// Before Selection Instance
//*****************************
$dwz_loop = 0;
$dwzLoopName = "Repeat2";
$dwzMaxRec = "-1";
do{
$dwz_loop += 1;
//End Repeat region
?>
<?php
$json = dwzGetRecValue('Recordset2','form');
$obj = json_decode($json);
$checkin = dwzGetRecValue('Recordset2','check_in');
$checkout = dwzGetRecValue('Recordset2','check_out');
?>
    <tr>
      <th width="55" height="30" scope="col"><?php echo dwzGetRecValue2('Recordset2','calendar_id'); ?></th>
      <th width="138" height="30" scope="col"><?php echo date('d-m-Y', strtotime($checkin)); ?></th>
      <th width="132" height="30" scope="col"><?php echo date('d-m-Y', strtotime($checkout)); ?></th>
      <th width="238" height="30" scope="col">
	  <?php echo $obj->{'wpdevart_form_field1'}; ?>
	  <?php echo $obj->{'wpdevart_form_field2'}; ?></th>
      <th width="213" height="30" scope="col"><?php echo $obj->{'wpdevart_form_field4'}; ?></th>
    </tr>
    <?php
//*****************************
// ProfRecWiz
// Repeat Region
// After Selection Instance
//*****************************
$dwzLoopName = "Repeat2";
if($dwzMaxRec != "" && $dwzMaxRec != "-1"){
	if(intval($dwz_loop) >= intval($dwzMaxRec)){
		break;
	}
}
}while($row_Recordset2 = mysqli_fetch_assoc($Recordset2));
//End Repeat region
?>
  <tr>
    
  </tr>
</table>
<p>&nbsp;</p>

<?php
//**************************
// ProfRecWiz - MySqli
//**************************
mysqli_free_result($Recordset2);
//**************************
// ProfRecWiz - MySqli
//**************************
?>
<?php 
//**************************
// ProfRecWiz
//**************************
function dwzGetRecValue2($rec, $field){
	if(isset($GLOBALS['row_'.$rec])){
		return $GLOBALS['row_'.$rec][$field];
	}else{
		return $rec .'.' .$field;
	}
}
//**************************
// ProfRecWiz
//**************************

?>

<h2>#3: Wassmelter</h2>

<?php
//**************************
// ProfRecWiz - MySqli
// http://www.dwzone-it.com
// Version: 1.0.15
//**************************
mysqli_select_db($lokaal_ddd, $database_lokaal_ddd);
$mysqli_Recordset3_query = "SELECT bijen_wpdevart_reservations.calendar_id, bijen_wpdevart_reservations.check_in, bijen_wpdevart_reservations.check_out, bijen_wpdevart_reservations.form FROM bijen_wpdevart_reservations WHERE (((bijen_wpdevart_reservations.calendar_id)=3) AND ((bijen_wpdevart_reservations.check_in)>" .GetSQLiValueString($lokaal_ddd, "2018-01-01", "text") .")) ORDER BY bijen_wpdevart_reservations.check_in Desc";
$Recordset3 = mysqli_query($lokaal_ddd, $mysqli_Recordset3_query) or die(mysqli_error($lokaal_ddd));
$row_Recordset3 = mysqli_fetch_assoc($Recordset3);
$totalRows_Recordset3 = mysqli_num_rows($Recordset3);
//**************************
// ProfRecWiz - MySqli
//**************************
?>

<table width="810" border="1">
  <tr>      
      <th width="55" height="30" scope="col"><strong>#</strong></th>
      <th width="139" height="30" scope="col"><strong>Begindatum</strong></th>
      <th width="135" height="30" scope="col"><strong>Einddatum</strong></th>
      <th width="234" height="30" scope="col"><strong>Naam</strong></th>
      <th width="213" height="30" scope="col"><strong>Telefoon</strong></th>
  </tr>
  <?php
//*****************************
// ProfRecWiz
// Repeat Region
// Before Selection Instance
//*****************************
$dwz_loop = 0;
$dwzLoopName = "Repeat3";
$dwzMaxRec = "-1";
do{
$dwz_loop += 1;
//End Repeat region
?>
<?php
$json = dwzGetRecValue('Recordset3','form');
$obj = json_decode($json);
$checkin = dwzGetRecValue('Recordset3','check_in');
$checkout = dwzGetRecValue('Recordset3','check_out');
?>
    <tr>
      <th width="55" height="30" scope="col"><?php echo dwzGetRecValue3('Recordset3','calendar_id'); ?></th>
      <th width="139" height="30" scope="col"><?php echo date('d-m-Y', strtotime($checkin)); ?></th>
      <th width="135" height="30" scope="col"><?php echo date('d-m-Y', strtotime($checkout)); ?></th>
      <th width="234" height="30" scope="col">
	  <?php echo $obj->{'wpdevart_form_field1'}; ?>
	  <?php echo $obj->{'wpdevart_form_field2'}; ?></th>
      <th width="213" height="30" scope="col"><?php echo $obj->{'wpdevart_form_field4'}; ?></th>
    </tr>
    <?php
//*****************************
// ProfRecWiz
// Repeat Region
// After Selection Instance
//*****************************
$dwzLoopName = "Repeat3";
if($dwzMaxRec != "" && $dwzMaxRec != "-1"){
	if(intval($dwz_loop) >= intval($dwzMaxRec)){
		break;
	}
}
}while($row_Recordset3 = mysqli_fetch_assoc($Recordset3));
//End Repeat region
?>
  <tr>
    
  </tr>
</table>
<p>&nbsp;</p>

<?php
//**************************
// ProfRecWiz - MySqli
//**************************
mysqli_free_result($Recordset3);
//**************************
// ProfRecWiz - MySqli
//**************************
?>
<?php 
//**************************
// ProfRecWiz
//**************************
function dwzGetRecValue3($rec, $field){
	if(isset($GLOBALS['row_'.$rec])){
		return $GLOBALS['row_'.$rec][$field];
	}else{
		return $rec .'.' .$field;
	}
}
//**************************
// ProfRecWiz
//**************************

?>
</body>






						</div>
					<!-- skepost --> 
					</div>
					<!-- post -->
					<?php edit_post_link( __('Edit', 'advertica-lite') , '', ''); ?>

						<div class="clearfix"></div>
				</div>
				<!-- content -->

				<!-- Sidebar -->
				<div id="sidebar" class="span3">
					<?php get_sidebar('page'); ?>
				</div>
				<div class="clearfix"></div>
				<!-- Sidebar --> 
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>